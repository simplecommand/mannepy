# Typisierung von Variablen mit Type Hints
zahl: int = 5
name: str = "Alice"
ist_schueler: bool = True
noten: list = [1, 2, 3, 4]
koordinaten: tuple = (10.0, 20.0)
studenten_info: dict = {"name": "Bob", "alter": 25}

def main():
    print("Hello World")

if __name__ == "__main__":
    main()
