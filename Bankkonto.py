class Bankkonto:
    def __init__(self, inhaber, saldo=0):
        self.inhaber = inhaber
        self.__saldo = saldo  # Dies ist eine private Variable

    def einzahlen(self, betrag):
        if betrag > 0:
            self.__saldo += betrag
            print(f"{betrag}€ eingezahlt.")
        else:
            print("Der Einzahlungsbetrag muss positiv sein.")

    def auszahlen(self, betrag):
        if betrag > 0 and betrag <= self.__saldo:
            self.__saldo -= betrag
            print(f"{betrag}€ ausgezahlt.")
        else:
            print("Ungültiger Auszahlungsbetrag.")

    def kontostand_anzeigen(self):
        print(f"Der aktuelle Saldo ist {self.__saldo}€.")

    def __interne_transaktion(self):
        # Private Methode, nur innerhalb der Klasse benutzt
        print("Interne Transaktion durchgeführt.")

    def transaktion_durchfuehren(self):
        self.einzahlen(100)
        self.__interne_transaktion()
        self.auszahlen(50)

# Beispiel der Nutzung:
konto = Bankkonto("Max Mustermann")
konto.einzahlen(200)
konto.auszahlen(50)
konto.kontostand_anzeigen()
konto.transaktion_durchfuehren()