from flask import Flask, render_template, request
from flask_basicauth import BasicAuth
import random

def passwortGenerator(nr_letters, nr_symbols, nr_numbers) :

    letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
    numbers = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
    symbols = ['!', '#', '$', '%', '&', '(', ')', '*', '+']

    password_list = []

    for char in range(1, nr_letters + 1):
        password_list.append(random.choice(letters))

    for char in range(1, nr_symbols + 1):
        password_list += random.choice(symbols)

    for char in range(1, nr_numbers + 1):
        password_list += random.choice(numbers)

    random.shuffle(password_list)
    password = ""
    for char in password_list:
        password += char

    return password


app = Flask(__name__)

app.config['BASIC_AUTH_USERNAME'] = 'admin'
app.config['BASIC_AUTH_PASSWORD'] = 'secret'
app.config['BASIC_AUTH_FORCE'] = True  # Erzwingt Basic Auth für alle Routen

basic_auth = BasicAuth(app)

@app.route('/', methods=['GET', 'POST'])
def home():
    password = ''
    if request.method == 'POST':
        no_letters = request.form.get('length', 0, type=int)
        no_numbers = request.form.get('zahlen', 0, type=int)
        no_symbols = request.form.get('sonder', 0, type=int)
        password = passwortGenerator(no_letters, no_numbers, no_symbols)

    return render_template('index.html', password=password)

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5001, ssl_context=('cert.pem', 'key.pem'))

