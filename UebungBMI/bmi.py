def bmiCalculator() :

    # Stufe 1
    height = input("Gebe Deine Größe in cm ein: ")
    weight = input("Gebe Dein Gewicht in kg ein: ")

    weight_as_int = int(weight)
    height_as_float = float(height) / 100

    bmi = weight_as_int / height_as_float ** 2

    bmi_as_int = int(bmi)
    print(bmi_as_int)

    # Stufe 2

def main():
    #passwortGenerator()
    bmiCalculator()

if __name__ == "__main__":
    main()